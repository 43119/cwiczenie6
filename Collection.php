<?php
namespace Vendor\Module\Model\ResourceModel\Example;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            \Vendor\Module\Model\Example::class,
            \Vendor\Module\Model\ResourceModel\Example::class
        );
    }
}